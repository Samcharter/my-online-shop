<?php
    namespace CSF\Repositories;

    use CSF\Repositories\Database;
    

	class UserRepository {

        private $collection = "devdb.users";

        /**
        * @Inject
        * @var Database
        */
        private $database;

        public function __construct() { }

        public function validate($user){
            if (password_verify($user->getPassword(), $this->getInfo($user)->hash) == true ) {
                $filter = [
                    'login' => ['$eq' => $user->getLogin()],                 
                ];
                $query = $this->database->getQuery($filter);
                $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
                return (count($cursor->toArray()) > 0);
                
            }
        }

        public function getAll($login){
            $filter = [
                'login' => ['$eq' => $login]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $adress = Adress::jsonDeserialize($result);
                array_push($results, $adress);
            }
            return $results;
        }

        public function getAllEmail($email){
            $filter = [
                'email' => ['$eq' => $email]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $adress = Adress::jsonDeserialize($result);
                array_push($results, $adress);
            }
            return $results;
        }

        public function getInfo($user){
            $filter = [
                'login' => ['$eq' => $user->getLogin()],
               
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $info = $cursor->toArray()[0];
            $result = User::jsonDeserialize($info);
            return $result;
        }

        

        public function create($user){
            $bulk = $this->database->getBulkWrite();
            $bulk->insert($user->jsonSerialize());
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getInsertedCount() > 0);
        }

        

    }
?>

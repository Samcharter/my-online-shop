<?php
    namespace CSF\Repositories;

    use \JsonSerializable;
    use CSF\Repositories\Database;

    class Shop implements \JsonSerializable{

        /**
        * @Inject
        * @var Database
        */
        private $database;

        public $_id = null;
        public $itemName = null;
        public $itemCategory = null;
        public $itemPrice = null;
        public $itemDescription = null;
        public $itemPicURL = null;
        public $login = null;
        public $qt = null;

        public function __construct() {}

        public function getId() { return $this->_id; }
        public function getItemName() { return $this->itemName; }
        public function getItemCategory() { return $this->itemCategory; }
        public function getItemPrice() { return $this->itemPrice; }
        public function getItemDescription() { return $this->itemDescription; }
        public function getItemPicURL() { return $this->itemPicURL; }
        public function getLogin() { return $this->login; }
        public function getQt() { return $this->qt; }

        public function setId($value) { $this->_id  = $value; }
        public function setItemName($value) { $this->itemName = $value; }
        public function setItemCategory($value) { $this->itemCategory = $value; }
        public function setItemPrice($value) { $this->itemPrice = $value; }
        public function setItemDescription($value) { $this->itemDescription = $value; }
        public function setItemPicURL($value) { $this->itemPicURL = $value; }
        public function setLogin($value) { $this->login = $value; }
        public function setQt($value) { $this->qt = $value; }


        public function jsonSerialize() {
            return [
                //'_id' => $this->database->generateId($this->_id),
                'itemName' => $this->itemName,
                'itemCategory' => $this->itemCategory,
                'itemPrice' => $this->itemPrice,
                'itemDescription' => $this->itemDescription,
                'itemPicURL' => $this->itemPicURL,
                'login' => $this->login,
                'qt' => $this->qt
            ];
        }

        public static function jsonDeserialize($json){
            $instance = new self();
            foreach($json as $key => $value){
                if ($key == '_id') {
                    $instance->{$key} = $value->__toString();
                } else {
                    $instance->{$key} = $value;   
                }
            }
            return $instance;
        }
    }


?>
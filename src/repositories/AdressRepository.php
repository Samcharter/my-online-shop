<?php

    namespace CSF\Repositories;

    class AdressRepository {

        private $collection = "devdb.adresses";

        /**
        * @Inject
        * @var Database
        */
        private $database;

        public function __construct() { }

        public function create($adress){
            $bulk = $this->database->getBulkWrite();
            $bulk->insert($adress->jsonSerialize());
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getInsertedCount() > 0);
        }

        public function getAll($login){
            $filter = [
                'login' => ['$eq' => $login]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $adress = Adress::jsonDeserialize($result);
                array_push($results, $adress);
            }
            return $results;
        }

        public function get($id){
            $filter = [
                '_id' => ['$eq' => $this->database->generateId($id)]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $adress = $cursor->toArray()[0];
            $result = Adress::jsonDeserialize($adress);
            return $result;
        }

        public function delete($adress){
            $bulk = $this->database->getBulkWrite();
            $bulk->delete(['_id' => $this->database->generateId($adress->getId())]);
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getDeletedCount() > 0);
        }

        public function update($task){
            $bulk = $this->database->getBulkWrite();
            $bulk->update(['_id' => $this->database->generateId($task->getId())], [
                '$set' => [
                    'nom' => $task->getNom(), 
                    'description' => $task->getDescription(),
                    'date' => $task->getDate(),
                    'completed' => $task->getCompleted()
                ]
            ]);
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getModifiedCount() > 0);
        }
    }

    ?>
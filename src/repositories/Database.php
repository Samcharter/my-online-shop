<?php
    namespace CSF\Repositories;

    use MongoDB\Driver\Command;
    use MongoDB\Driver\Manager;
    use MongoDB\Driver\Query;
    use MongoDB\Driver\BulkWrite;
    use MongoDB\BSON\ObjectID;

	class Database {

        private static $uriOptions = [
            'username' => 'admin',
            'password' => 'admin123',
            'authMechanism' => 'SCRAM-SHA-1'
        ];

        public function __construct() { }

        public function getManager() {
            $manager = new Manager("mongodb://localhost:27017/devdb", self::$uriOptions);
            $manager->executeCommand('devdb', new Command(['ping' => 1]));
            return $manager;
        }

        public function getBulkWrite() {
            return new BulkWrite;
        }

        public function getQuery($filter, $options = null) {
            return new Query($filter, $options);
        }

        public function generateId($param) {
            return new ObjectID($param);
        }
    }
?>

<?php
    namespace CSF\Repositories;

    use \JsonSerializable;

    class Adress implements \JsonSerializable{

        private $_id = null;
        public $adressFN = null;
        public $adressLN = null;
        public $adressAD = null;
        public $adressCT = null;
        public $adressPC = null;
        public $adressPV = null;
        public $adressCN = null;
        private $login = null;

        public function __construct() {}

        public function getId() { return $this->_id; }
        public function getAdressFN() { return $this->adressFN; }
        public function getAdressLN() { return $this->adressLN; }
        public function getAdressAD() { return $this->adressAD; }
        public function getAdressCT() { return $this->adressCT; }
        public function getAdressPC() { return $this->adressPC; }
        public function getAdressPV() { return $this->adressPV; }
        public function getAdressCN() { return $this->adressCN; }
        public function getLogin() { return $this->login; }

        public function setId($value) { $this->_id = $value; }
        public function setAdress($value) { $this->adress = $value; }
        public function setAdressFN($value) { $this->adressFN = $value; }
        public function setAdressLN($value) { $this->adressLN = $value; }
        public function setAdressAD($value) { $this->adressAD = $value; }
        public function setAdressCT($value) { $this->adressCT = $value; }
        public function setAdressPC($value) { $this->adressPC = $value; }
        public function setAdressPV($value) { $this->adressPV = $value; }
        public function setAdressCN($value) { $this->adressCN = $value; }
        public function setLogin($value) { $this->login = $value; }

        public function jsonSerialize() {
            return [
                'adressFN' => $this->adressFN,
                'adressLN' => $this->adressLN,
                'adressAD' => $this->adressAD,
                'adressCT' => $this->adressCT,
                'adressPC' => $this->adressPC,
                'adressPV' => $this->adressPV,
                'adressCN' => $this->adressCN,
                'login' => $this->login
            ];
        }

        public static function jsonDeserialize($json){
            $instance = new self();
            foreach($json as $key => $value){
                if ($key == '_id') {
                    $instance->{$key} = $value->__toString();
                } else {
                    $instance->{$key} = $value;   
                }
            }
            return $instance;
        }

    }


?>
<?php 
    namespace CSF\Repositories;

    class ShopRepository{

        private $collection = "devdb.items";
        private $cart = "devdb.cart";
        private $wishlist = "devdb.wishlist";

        /**
        * @Inject
        * @var Database
        */
        private $database;

        public function __construct() { }

        public function create($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->insert($item->jsonSerialize());
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getInsertedCount() > 0);
        }

        public function addToCart($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->insert($item->jsonSerialize());
            $result = $this->database->getManager()->executeBulkWrite($this->cart, $bulk);
            return ($result->getInsertedCount() > 0);
        }

        public function addToWishList($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->insert($item->jsonSerialize());
            $result = $this->database->getManager()->executeBulkWrite($this->wishlist, $bulk);
            return ($result->getInsertedCount() > 0);
        }

        public function getAllWishlistItems($login){
            $filter = [
                'login' => ['$eq' => $login]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->wishlist, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $shop = Shop::jsonDeserialize($result);
                array_push($results, $shop);
            }
            return $results;
        }

        public function getAllItems($login){
            $filter = [
                'login' => ['$eq' => $login]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $shop = Shop::jsonDeserialize($result);
                array_push($results, $shop);
            }
            return $results;
        }

        public function getAllCartItems($login){
            $filter = [
                'login' => ['$eq' => $login]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->cart, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $shop = Shop::jsonDeserialize($result);
                array_push($results, $shop);
            }
            return $results;
        }

        public function getAllCategoryItems($category){
            $filter = [
                'itemCategory' => ['$eq' => $category]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $shop = Shop::jsonDeserialize($result);
                array_push($results, $shop);
            }
            return $results;
        }

        public function getAllSearchItems($value){
            $filter = [
                'itemName' => ['$eq' => $value]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $results = array();
            foreach ($cursor as $result) {
                $shop = Shop::jsonDeserialize($result);
                array_push($results, $shop);
            }
            return $results;
        }

        public function get($id){
            $filter = [
                '_id' => ['$eq' => $this->database->generateId($id)]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $item = $cursor->toArray()[0];
            $result = Shop::jsonDeserialize($item);
            return $result;
        }

        public function getShopItemByName($name){
            $filter = [
                'itemName' => ['$eq' => $name ]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->collection, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $item = $cursor->toArray()[0];
            $result = Shop::jsonDeserialize($item);
            return $result;
        }

        

        public function getCartItemById($id){
            $filter = [
                '_id' => ['$eq' => $this->database->generateId($id)]
            ];
            $query = $this->database->getQuery($filter);
            $cursor = $this->database->getManager()->executeQuery($this->cart, $query);
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            $item = $cursor->toArray()[0];
            $result = Shop::jsonDeserialize($item);
            return $result;
        }

        

       

        public function delete($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->delete(['_id' => $this->database->generateId($item->getId())]);
            $result = $this->database->getManager()->executeBulkWrite($this->cart, $bulk);
            return ($result->getDeletedCount() > 0);
        }

        public function update($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->update(['_id' => $this->database->generateId($item->getId())], [
                '$set' => [
                    'itemName' => $item->getItemName(), 
                    'itemCategory' => $item->getItemCategory(),
                    'itemPrice' => $item->getItemPrice(),
                    'itemDescription' => $item->getItemDescription(),
                    'itemPicURL' => $item->getItemPicURL()
                ]
            ]);
            $result = $this->database->getManager()->executeBulkWrite($this->collection, $bulk);
            return ($result->getModifiedCount() > 0);
        }

        public function updateCart($item){
            $bulk = $this->database->getBulkWrite();
            $bulk->update(['_id' => $this->database->generateId($item->getId())], [
                '$set' => [
                    'itemName' => $item->getItemName(), 
                    'itemCategory' => $item->getItemCategory(),
                    'itemPrice' => $item->getItemPrice(),
                    'itemDescription' => $item->getItemDescription(),
                    'itemPicURL' => $item->getItemPicURL(),
                    'qt' => $item->getQt()
                ]
            ]);
            $result = $this->database->getManager()->executeBulkWrite($this->cart, $bulk);
            return ($result->getModifiedCount() > 0);
        }

    }
?>
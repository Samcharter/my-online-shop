<?php 

    namespace CSF\Repositories;

    use \JsonSerializable;

    class User implements \JsonSerializable{

        public $login = null;
        public $firstname = null;
        public $lastname = null;
        public $email = null;
        private $password = null;
        public $hash = null;
        private $confirmpassword = null;
        private $subscriptiondate = null;
        
        

        

        public function __construct() {}

        public function getLogin() { return $this->login; }
        public function getFirstname() { return $this->firstname; }
        public function getLastname() { return $this->lastname; }
        public function getEmail() { return $this->email; }
        public function getPassword() { return $this->password; }
        public function getHash() { return $this->hash; }
        public function getConfirmPassword() { return $this->confirmpassword; }
        
        

        public function setLogin($value) { $this->login = $value; }
        public function setFirstname($value) { $this->firstname = $value; }
        public function setLastname($value) { $this->lastname = $value; }
        public function setEmail($value) { $this->email = $value; }
        public function setPassword($value) { $this->password = $value; }
        public function setHash($value) { $this->hash = $value; }
        public function setConfirmPassword($value) { $this->confirmpassword = $value; }
        public function setSubDate() {$this->subscriptiondate = date("d/m/y"); }
        
        

        public function jsonSerialize() {
            return [
                'login' => $this->login,
                'firstname' => $this->firstname,
                'lastname' => $this->lastname,
                'email' => $this->email,
                //'password' => $this->password,
                'hash' => $this->hash,
                //'confirmpassword' => password_hash($this->confirmpassword, PASSWORD_DEFAULT),
                'subdate' => $this->subscriptiondate,
    
            ];
        }

        public static function jsonDeserialize($json){
            $instance = new self();
            foreach($json as $key => $value){
                if ($key == '_id') {
                    $instance->{$key} = $value->__toString();
                } else {
                    $instance->{$key} = $value;   
                }
            }
            return $instance;
        }
        
    }
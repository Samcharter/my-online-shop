<?php


  namespace CSF;

  use CSF\Controllers\UserController;
  use CSF\Controllers\ShopController;
  use CSF\Controllers\AdressController;

  class Router {
      /**
       * @Inject
       * @var UserController
       */
      private $userController;
      /**
       * @Inject
       * @var AdressController
       */
      private $adressController;
      /**
       * @Inject
       * @var ShopController
       */
      private $shopController;



      public $controllers = array(
        'user' => ['home', 'signin', 'requestSignin', 'signup', 'requestSignup', 'signout', 'error', 'profile', 'edit', 'requestEdit'],
        'shop' => ['showCart', 'addToCart', 'deleteFromCart', 'deleteAll','editQt','addToWishlist', 'showWishlist', 'browse','showItem', 'adminShowItems','addItem','edit', 'removeItem','save', 'search'],
        'adress' => ['add', 'edit', 'save', 'show', 'delete']
      );

      private function call($controller, $action) {
        switch ($controller) {
          case 'adress':
            $controller = $this->adressController;
            break;
          case 'shop':
            $controller = $this->shopController;
            break;
          default:
            $controller = $this->userController;
            break;
        }
        $controller->{ $action }();
      }

      public function process() {
          if (isset($_GET['controller']) && isset($_GET['action'])) {
              $controller = strip_tags($_GET['controller']);
              $action = strip_tags($_GET['action']);
          } else {
              $controller = 'user';
              $action = 'home';
          }
          if (array_key_exists($controller, $this->controllers)) {
              if (in_array($action, $this->controllers[$controller])) {
                  if ($action == 'profile' && !$this->is_authenticated()) {
                      $this->call('user','requestSignin');
                  } else if ($action == 'signout' && !$this->is_authenticated()){
                      $this->call('user','home');
                  } else if (($action == 'requestSignin' || $action == 'requstSignup') && $this->is_authenticated()){
                      $this->call('user', 'home');
                      echo "allô";
                  }
                  else if ($controller == "user" or $this->is_authenticated()) {
                      $this->call($controller, $action);
                  } else if($controller == "shop") {
                    $this->call($controller, $action);
                  } else {
                      $this->call('user', 'requestSignin');
                  }
              } else {
                  $this->call('user', 'home');
              }
          } else {
              $this->call('user', 'error');
          }
      }

      private function is_authenticated() {
          return (isset($_SESSION['user']) && (time() - $_SESSION['timestamp'] <= 999999999));
      }
  }







?>

<?php
    namespace CSF\Controllers;

    use CSF\Views\AdressView;
    use CSF\Views\AdressListView;
    use CSF\Models\AdressModel;
    use CSF\Repositories\AdressRepository;
    use CSF\Controllers\UserController;


    class AdressController {
        /**
       * @Inject
       * @var AdressView
       */
      private $adressView;
      /**
       * @Inject
       * @var AdressListView
       */
      private $adressListView;
      /**
       * @Inject
       * @var AdressModel
       */
      private $model;
      /**
       * @Inject
       * @var AdressRepository
       */
      private $service;
      /**
       * @Inject
       * @var UserController
       */
      private $userController;
 

      public function __construct()
      {
      }


      public function add(){
        $this->adressView->render();
      }

      public function show(){
        $this->model->setUser($_SESSION['user']);
        $adressList = $this->service->getAll($this->model->getUser()->getLogin());
        $this->model->setAdressList($adressList);
        $this->adressListView->render();
      }

      public function save(){
        //if ($_SERVER['REQUEST_URI'] == "/projet/index.php?controller=adress&action=add"){
          $this->model->setUser($_SESSION['user']);
          if (isset($_POST['id'])) { 
            $this->model->getAdress()->setId(strip_tags($_POST['id']));
          }
          $this->model->getAdress()->setAdressFN(strip_tags($_POST['firstName']));
          $this->model->getAdress()->setAdressLN(strip_tags($_POST['lastname']));
          $this->model->getAdress()->setAdressAD(strip_tags($_POST['adress']));
          $this->model->getAdress()->setAdressCT(strip_tags($_POST['city']));
          $this->model->getAdress()->setAdressPC(strip_tags($_POST['postalCode']));
          $this->model->getAdress()->setAdressPV(strip_tags($_POST['province']));
          $this->model->getAdress()->setAdressCN(strip_tags($_POST['country']));
          $this->model->getAdress()->setLogin($this->model->getUser()->getLogin());
          if (!isset($_POST['id'])) {
            if ($this->service->create($this->model->getAdress())) {
              $this->model->setMessage("L'adresse a été ajoutée avec succès.");
            } else {
              $this->model->setMessage("L'adresse n'a pas pu être ajoutée.");
            }
          } else {
            if ($this->service->update($this->model->getAdress())) {
              $this->model->setMessage("L'adresse a été modifiée avec succès.");
            } else {
              $this->model->setMessage("L'adresse n'a pas pu être modifiée.");
            }
          }
          $this->show();
        //}else { $this->show(); }
        
      }

      public function edit(){
        $this->adressView->render();
      }

      public function delete(){
        $this->model->getAdress()->setId(strip_tags($_GET['id']));
        if ($this->service->delete($this->model->getAdress())) {
          $this->model->setMessage("La tâche a été supprimée avec succès.");
        } else {
          $this->model->setMessage("La tâche n'a pas pu être supprimée.");
        }
        $this->show();
      }
      
    }


?>
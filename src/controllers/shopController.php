<?php
  namespace CSF\Controllers;

  use CSF\Controllers\UserController;
  use CSF\Controllers\ShopController;
  use CSF\Views\ShopView;
  use CSF\Views\CartView;
  use CSF\Views\ItemView;
  use CSF\Models\ShopModel;
  use CSF\Repositories\ShopRepository;

  class ShopController {
    /**
        * @Inject
        * @var ShopView
        */
        private $shopView;
        /**
        * @Inject
        * @var CartView
        */
        private $cartView;
        /**
        * @Inject
        * @var ItemView
        */
        private $itemView;
        /**
        * @Inject
        * @var ShopModel
        */
        private $model;
        /**
        * @Inject
        * @var ShopRepository
        */
        private $service;
        /**
        * @Inject
        * @var UserController
        */
        private $userController;

    public function __contruct(){}
  

    function browse() {
      $this->shopView->renderHome();  
    }


    public function adminShowItems(){
      $this->shopView->adminShowItems();
    }

    public function addItem(){
      $this->shopView->renderAdd();
    }

    public function edit(){
      $this->shopView->renderAdd();
    }

    public function save(){
      
      if (isset($_POST['id'])) { 
        $this->model->getShop()->setId(strip_tags($_POST['id']));
      }
      $this->model->getShop()->setItemName(strip_tags($_POST['itemName']));
      $this->model->getShop()->setItemCategory(strip_tags($_POST['itemCategory']));
      $this->model->getShop()->setItemPrice(strip_tags($_POST['itemPrice']));
      

      $URLs = strip_tags($_POST['itemPicURL']);
      $URLarray = explode(",", $URLs);

      $description = strip_tags($_POST['itemDescription']);
      $descriptionArray = explode(".", $description);



      $this->model->getShop()->setItemDescription($descriptionArray);
      $this->model->getShop()->setItemPicURL($URLarray);
      $this->model->getShop()->setLogin('admin');
      if (!isset($_POST['id'])){
        $this->service->create($this->model->getShop());                 
      } else {
        $this->service->update($this->model->getShop());            
      }
      $this->adminShowItems();   
    }

    public function editQt(){
      
      $item = $this->service->getCartItemById($_GET['id']);
      
      $this->model->getShop()->setId($_GET['id']);     
      $this->model->getShop()->setItemName($item->itemName);
      $this->model->getShop()->setItemCategory($item->itemCategory);
      $this->model->getShop()->setItemPrice($item->itemPrice);
      $this->model->getShop()->setItemDescription($item->itemDescription);
      $this->model->getShop()->setItemPicURL($item->itemPicURL);
      $this->model->getShop()->setQt($_POST['qt']);

      $this->model->getShop()->setLogin($item->login);
      
      $this->service->updateCart($this->model->getShop());        
      
      $this->showCart();
    }

    public function addToCart(){
      $itemID = $_GET['id'];
      $isInCart = false;
      
      if (isset($_SESSION['user'])){
        $cartItems = $this->service->getAllCartItems($_SESSION['user']->login);
        foreach($cartItems as $item){
          if ($item->itemName == $this->service->get($itemID)->itemName){
            $isInCart = true;
            $this->model->setMessage("Cet article est déjà dans votre panier");
            break;
          }
        }

        if ($isInCart !== true){
          $this->model->getShop()->setQt($_POST['quantity']);
          $this->model->getShop()->setId($itemID);
          $this->model->getShop()->setItemName($this->service->get($itemID)->itemName);
          $this->model->getShop()->setItemCategory($this->service->get($itemID)->itemCategory);
          $this->model->getShop()->setItemPrice($this->service->get($itemID)->itemPrice);
          $this->model->getShop()->setItemDescription($this->service->get($itemID)->itemDescription);
          $this->model->getShop()->setItemPicURL($this->service->get($itemID)->itemPicURL);
          $this->model->getShop()->setLogin($_SESSION['user']->login);

          if ($this->service->addToCart($this->model->getShop())){
            $this->model->setMessage("L'article à bien été ajouté à votre panier");
          } else {
            $this->model->setMessage("L'article n'a pas être ajouté à votre panier");
          }
        }
        if($_GET['page'] == "shop"){
          $this->browse();
        } elseif($_GET['page'] == "cart"){
          $this->showCart();
        } elseif($_GET['page'] == "item"){
          $this->showItem();
        }
      } else {
        $this->model->setMessage("Veuillez vous connecter pour ajouter des articles à votre panier");
        $this->userController->requestSignin();
      }
        
        
      
    }

    public function addToWishlist(){
      $itemID = $_GET['id'];
      $isInWishlist = false; 


      if(isset($_SESSION['user'])){
        $wishlistItems = $this->service->getAllWishlistItems($_SESSION['user']->login);
       
        foreach($wishlistItems as $item){
          if ($item->itemName == $this->service->get($itemID)->itemName){
            $isInWishlist = true;
            $this->model->setMessage("Cet article est déjà dans votre liste de souhaits");
            break;
          }
        }

        if ($isInWishlist !== true){
          
          $this->model->getShop()->setId($itemID);
          $this->model->getShop()->setItemName($this->service->get($itemID)->itemName);
          $this->model->getShop()->setItemCategory($this->service->get($itemID)->itemCategory);
          $this->model->getShop()->setItemPrice($this->service->get($itemID)->itemPrice);
          $this->model->getShop()->setItemDescription($this->service->get($itemID)->itemDescription);
          $this->model->getShop()->setItemPicURL($this->service->get($itemID)->itemPicURL);
          $this->model->getShop()->setLogin($_SESSION['user']->login);

          if ($this->service->addToWishList($this->model->getShop())){
            $this->model->setMessage("L'article à bien été ajouté à votre liste de souhaits");
          } else {
            $this->model->setMessage("L'article n'a pas été ajouté à votre liste de souhaits");
          }
        }

        $this->showItem();

      } else {
        $this->model->setMessage("Veuillez vous connecter pour ajouter des articles à votre liste de souhaits ");
        $this->userController->requestSignin();
      }
    }

    public function showWishlist(){
      $this->shopView->renderWishlist();
    }

    

    public function deleteFromCart(){
      $itemID = $_GET['id'];
      $this->model->getShop()->setId($itemID);
      
      if($this->service->delete($this->model->getShop())){
        $this->model->setMessage("L'article à été retiré du panier avec succès");
        $this->showCart();
      } else {
        $this->model->setMessage("L'article n'a pas pu être retiré du panier");
        $this->showCart();
      }

    }

    public function deleteAll(){
      $cartItems = $this->service->getAllCartItems($_SESSION['user']->login);
      foreach($cartItems as $item){
        $this->model->getShop()->setId($item->_id);
        $this->service->delete($this->model->getShop());
      }
      $this->browse();
    }


    public function search(){
      $items = $this->service->getAllSearchItems(strip_tags($_POST['userSearch']));
      var_dump($items);
    }

    public function showCart(){
      $this->cartView->render();
    }

    public function showItem(){
        //$item2 = $this->service->get($_GET['id']);
      if ($_GET['page'] == "cart"){
        $item = $this->service->getCartItemById($_GET['id']);
        $item2 = $this->service->getShopItemByName($item->itemName);
       
      } else /*($_GET['page'] == "home" || $_GET['page'] == "shop")*/{
        $item2 = $this->service->get($_GET['id']);
      }
      
      
      $this->itemView->render($this->service->get($item2->_id));
      
    }
  }

?>

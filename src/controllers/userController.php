<?php
    namespace CSF\Controllers;

    use CSF\Views\HomeView;
    use CSF\Views\SigninView;
    use CSF\Views\SignupView;
    use CSF\Views\ProfileView;
    use CSF\Models\UserModel;
    use CSF\Repositories\UserRepository;
    use CSF\Views\BaseView;
    

    class UserController
    {
      /**
       * @Inject
       * @var BaseView
       */
      private $baseView;
      
      /**
       * @Inject
       * @var UserModel
       */
      private $model;
      /**
        * @Inject
        * @var UserRepository
        */
        private $service;
      /**
       * @Inject
       * @var HomeView
       */
      private $homeView;
      /**
       * @Inject
       * @var ProfileVIew
       */
      private $profileView;
      /**
       * @Inject
       * @var SigninView
       */
      private $signInView;
      /**
       * @Inject
       * @var SignupView
       */
      private $signUpView;


      public function __construct()
      {
        
      }

      public function home()
      { 
        $this->homeView->render();
      }

      public function profile(){
        $this->profileView->render();
      }

      public function requestSignin(){
        $this->signInView->render();
      }

      public function requestSignup(){
        $this->signUpView->render();
        
      }

      public function signUp(){
        if (!isset($_SESSION['user'])){
          $this->model->getUser()->setLogin(strip_tags($_POST['login']));
          $this->model->getUser()->setFirstname(strip_tags($_POST['firstname']));
          $this->model->getUser()->setLastname(strip_tags($_POST['lastname']));
          $this->model->getUser()->setEmail(strip_tags($_POST['email']));
          $this->model->getUser()->setPassword(strip_tags($_POST['password']));
          $this->model->getUser()->setHash(strip_tags(password_hash($_POST['password'], PASSWORD_DEFAULT)));
          $this->model->getUser()->setConfirmpassword(strip_tags($_POST['confirmpassword']));
          $this->model->getUser()->setSubDate();
          $this->model->validate();
          
          if ($this->model->getErrors() !== null) {
            $this->signUpView->render($this->model->getErrors());
          } elseif ($this->service->create($this->model->getUser())) {
            $_SESSION['user'] = $this->model->getUser();
            $_SESSION['timestamp'] = time();
            $this->home();
            
          }
        } else { $this->home(); }
      }

      public function signin(){
        $this->model->getUser()->setLogin(strip_tags($_POST['login']));
        $this->model->getUser()->setPassword(strip_tags($_POST['password']));
        if ($this->service->validate($this->model->getUser())) {
        
          $_SESSION['user'] = $this->model->getUser();
          $_SESSION['timestamp'] = time();
          $this->home();
          
        } else {
          $this->model->setErrors(["Votre nom d'utilisateur et/ou votre mot de passe est incorrect."]);
          $this->signInView->render();
        }
        

      }

      

      function signout(){
        session_unset();
        session_destroy();
        $this->home();
      }
    }

 ?>

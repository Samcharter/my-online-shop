<?php
    namespace CSF\Views;

    use CSF\Repositories\ShopRepository;

    


    class ShopView extends BaseView {

        /**
        * @Inject
        * @var ShopRepository
        */
        private $service;

        public function __construct(){}

        public function renderHome(){
            parent::render_navigation();
            parent::render_messages();
            
            ?>
            <div class="container-fluid pl-3 pt-2 pb-2">
                <div class="row" >
                    <div class="col m-2 pl-3 pt-2 pb-2" style="background-color:white;">
                        <h4 >Catégories</h4>
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "home") { echo "active"; } ?>" href="?controller=shop&action=browse&category=home">Home</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "electroniques") { echo "active"; } ?>" href="?controller=shop&action=browse&category=electroniques">Electroniques</a>           
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "meubles") { echo "active"; } ?>" href="?controller=shop&action=browse&category=meubles">Meubles</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "electromenagers") { echo "active"; } ?>" href="?controller=shop&action=browse&category=electromenagers">Electroménagers</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "livres") { echo "active"; } ?>" href="?controller=shop&action=browse&category=livres">Livres</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php if($_GET['category'] == "sports") { echo "active"; } ?>" href="?controller=shop&action=browse&category=sports">Sports</a>
                                
                            </li>
                            
                            
                        </ul>
                    </div>
                    <div id="myTabContent" class=" tab-content col-10 m-2" style="background-color:white;">
                        <?php 
                            if (isset($_GET["category"])){
                                if ($_GET["category"] == 'home'){ ?>
                                    
                                        <h1 class="display-3">Home</h1>
                                        <p class="lead">Vous trouverez ici plusieurs articles de toutes sortes! Pour trouver des articles en particulier, vous pouvez utiliser le menu sur la gauche pour choisir une catégorie.</p>
                                        <hr class="my-4">
                                        <div class="row d-flex justify-content-center" id="items">
                                            <?php $this->showAllItems(); ?>
                                        </div>
                                    
                        <?php 
                                } 
                                elseif($_GET["category"] == 'electroniques'){ ?> 
                                
                                    
                                        <h1 class="display-3">Electroniques</h1>
                                        <hr class="my-4">
                                        <div class="row d-flex justify-content-center" id="items">
                                        <?php $this->showAllCategoryItems("Électroniques"); ?>
                                        </div>                              
                                <?php }
                                elseif($_GET["category"] == 'meubles'){ ?>                       
                                    <h1 class="display-3">Meubles</h1>
                                    <hr class="my-4">
                                    <div class="row d-flex justify-content-center" id="items">
                                    <?php $this->showAllCategoryItems("Meubles"); ?>
                                    </div>                            
                            <?php }
                                elseif($_GET["category"] == 'electromenagers'){ ?>
                                    <h1 class="display-3">Electroménagers</h1>
                                            <hr class="my-4">
                                            <div class="row d-flex justify-content-center" id="items">
                                            <?php $this->showAllCategoryItems("Electroménagers"); ?>
                                            </div> 
                                    <?php 
                                }
                                elseif($_GET["category"] == 'livres'){ ?>
                                    <h1 class="display-3">Livres</h1>
                                            <hr class="my-4">
                                            <div class="row d-flex justify-content-center" id="items">
                                            <?php $this->showAllCategoryItems("Livres"); ?>
                                            </div> 
                                    <?php 
                                }
                                elseif($_GET["category"] == 'sports'){ ?>
                                    <h1 class="display-3">Sports</h1>
                                            <hr class="my-4">
                                            <div class="row d-flex justify-content-center" id="items">
                                            <?php $this->showAllCategoryItems("Sports"); ?>
                                            </div> 
                                    <?php 
                                }
                            } else {
                                ?>
                                <h1 class="display-3">Home</h1>
                                        <p class="lead">Vous trouverez ici plusieurs articles de toutes sortes! Pour trouver des articles en particulier, vous pouvez utiliser le menu sur la gauche pour choisir une catégorie.</p>
                                        <hr class="my-4">
                                        <div class="row d-flex justify-content-center" id="items">
                                            <?php $this->showAllItems(); ?>
                                        </div>
                                        <?php
                            }
                        ?>
                        
                        
                        
                    
                    
                    </div> 
                </div>
                

            </div>


            <?php 
            parent::render_copyright();
        }

        public function renderCategory($category = null){

        }
        
        public function adminShowItems(){
            parent::render_navigation();
            ?> 
            <div class="container-fluid pt-3">
                <h2>Articles</h2>
                <div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Catégorie</th>
                                <th scope="col">Prix</th>
                                <th scope="col">Description</th>
                                <th scope="col">URL</th>
                                <th scope="col">ID</th>
                                <th scope="col">OPTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $items = $this->service->getAllItems('admin');
                            foreach ($items as $item){
                               ?><tr>
                                        <th scope="row"><?php echo $item->itemName ?></th>
                                        <td> <?php echo $item->itemCategory ?></td>
                                        <td> <?php echo $item->itemPrice ?>$</td>
                                        <td> <?php foreach($item->itemDescription as $description) {echo $description."."; } ?></td>
                                        <td> <?php foreach($item->itemPicURL as $URL) {echo $URL.","; } ?></td>
                                        <td>'<?php echo $item->_id ?></td>
                                        <td><div class="btn-group p-2" role="group" aria-label="Basic example">
                                        <?php echo '<button type="button" class="btn btn-primary"><a href="?controller=shop&action=edit&id='.$item->_id.'"; style="color:white;">Modifier</a></button>
                                        <button type="button" class="btn btn-primary"><a href="?controller=shop&action=delete&id='.$item->_id.'"; style="color:white;">Supprimer</a></button>';?>
                                    </div></td>
                                    </tr><?php 
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
                <button type='button' class='btn btn-primary'><a href='?controller=shop&action=addItem' style='color:white;'>Ajouter un item</a></button>
            </div>
            <?php
        }

        public function renderWishlist(){
            parent::render_navigation();
            parent::render_messages();
            ?>



            <div class="container-fluid p-2 m-2" style="background-color: white;">
                'sup
            </div>

            <?php





        }

        public function renderAdd(){
            parent::render_navigation();
            ?>
            <div class="container pt-3">
                <form method="POST" id="addItemForm" action="?controller=shop&action=save">
                <?php
                    if ($_GET['action'] === "edit") {
                        $item = $this->service->get($_GET['id']);
                        
                        ?>

                            <input type="text" name="id" value="<?php if (isset($_GET['id'])) { print $_GET['id'];} ?>"/>
                        <?php
                    }

                ?>
                    <div class="form-group">
                        <label>Nom item</label>
                        <input type="text" class="form-control" name="itemName" id="itemName" value="<?php if (isset($item)) { print $item->itemName; } ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Catégorie item</label>
                        <input type="text" class="form-control" name="itemCategory" id="itemCategory" value="<?php if (isset($item)) { print $item->itemCategory; } ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Prix</label>
                        <input type="text" class="form-control" name="itemPrice" id="itemPrice" value="<?php if (isset($item)) { print $item->itemPrice; } ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Description (séparer chaque description par un point et un espace)</label>
                        <input type="text" class="form-control" name="itemDescription" id="itemDescription" value="<?php if (isset($item)) { 
                            foreach($item->itemDescription as $description) {
                                echo $description.". ";

                            }
                        } ?>" />
                    </div>
                    <div class="form-group">
                        <label>URL de la photo (séparer chaque URL par une virgule et un espace)</label>
                        <input type="text" class="form-control" name="itemPicURL" id="itemPicURL" value="<?php if (isset($item)) { 
                            foreach($item->itemPicURL as $URLs) {
                                echo $URLs.", ";

                            }
                         } ?>" />
                    </div>
                    <button class="btn btn-primary" id="okButton" >Ajouter</button>
                </form>
            </div>
            <?php
        }

        public function showAllItems(){
            $items = $this->service->getAllItems('admin');
            $i = 0;
            foreach ($items as $item) {
                if ($i % 3 == 0) {
                    ?> </div><div class='row d-flex justify-content-center'> <?php
                }
                ?> <div class='col jumbotron m-2 p-2 text-center' style='width:35rem'>
                <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $item->_id ?>' style="text-decoration: none; color: black;">
                    <h4><?php echo $item->itemName ?></h4>
                    <img src="<?php echo $item->itemPicURL[0] ?>" class='m-2' style='width:200px; height:200px;'/>
                </a>
                <ul class='text-left' style='list-style-type: none;'>
                    <li>Prix: <b><?php echo $item->itemPrice ?>$</b></li>
                    <li><small class='muted'><?php echo "<li>".$item->itemDescription[0]."</li><li>".$item->itemDescription[1]."</li>".$item->itemDescription[2]."</li>" ?></small></li>
                    <form method="POST" action="?controller=shop&action=addToCart&page=shop&id=<?php echo $item->_id?>">
                        <b>Qt:</b><select style="width: 70px;" class="custom-select" id="quantity" name="quantity">
                            
                            <option selected value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select> 
                        <button class='btn btn-warning'>Ajouter au panier</button>
                        <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $item->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                    </form>
                </ul>
            </div><?php 
                $i++;
                
            }
        }

        public function showAllCategoryItems($category){
            $items = $this->service->getAllCategoryItems($category);
            
            $i = 0;
            foreach ($items as $item) {
                if ($i % 3 == 0) {
                    ?> </div><div class='row d-flex justify-content-center'> <?php
                }
                ?> <div class='col jumbotron m-2 p-2 text-center' style='width:35rem'>
                <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $item->_id ?>' style="text-decoration: none; color: black;">
                    <h4><?php echo $item->itemName ?></h4>
                    <img src="<?php echo $item->itemPicURL[0] ?>" class='m-2' style='width:200px; height:200px;'/>
                </a>
                <ul class='text-left' style='list-style-type: none;'>
                    <li>Prix: <b><?php echo $item->itemPrice ?>$</b></li>
                    <li>
                        <small class='muted'><?php echo "<li>".$item->itemDescription[0]."</li><li>".$item->itemDescription[1]."</li>".$item->itemDescription[2]."</li>" ?></small>
                    </li>
                    <form method="POST" action="?controller=shop&action=addToCart&id=<?php echo $item->_id?>">
                        <b>Qt:</b><select style="width: 70px;" class="custom-select" id="quantity" name="quantity">
                            
                            <option selected value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select> 
                        <button class='btn btn-warning'>Ajouter au panier</button>
                        <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $item->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                    </form>
                    
                </ul>
            </div><?php 
                $i++;
                
            }
        }

        public function showCart(){
            $cartItems = $this->service->getAllCartItems($_SESSION['user']->login);
            return $cartItems;
        }

        

    }
?>
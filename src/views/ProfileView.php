<?php
    namespace CSF\Views;

    use CSF\Repositories\UserRepository;

    class ProfileView extends BaseView{
        /**
        * @Inject
        * @var UserRepository
        */
        private $service;
        /**
        * @Inject
        * @var AdressView
        */
        private $adressView;

        public function __construct(){}


        public function render(){
            parent::render_navigation();
            parent::render_messages();
            $infos = $this->service->getInfo($_SESSION['user']);
            ?>
         </div>
        <div class="container pt-3">
            <h2>Mon compte</h2>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Informations</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?controller=adress&action=show">Adresse(s)</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Achats</a>
                </li>          
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade show active pt-2" id="infos">
                    <div class="jumbotron" style="background-image: url(res/backgroundprofile2.jpg)">
                        <div class="form-group row ">
                            <label  style="color:white;" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" readonly="" style="color:white;" class="form-control-plaintext" id="staticEmail" value="<?php echo $infos->email; ?>">
                            </div>
                        </div>    
                    <div class="form-group row">
                        <label  style="color:white;" class="col-sm-2 col-form-label">Prénom</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="" style="color:white;" class="form-control-plaintext" id="staticEmail" value="<?php echo $infos->firstname; ?>">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label  style="color:white;" class="col-sm-2 col-form-label">Nom de famille</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="" style="color:white;" class="form-control-plaintext" id="staticEmail" value="<?php echo $infos->lastname; ?>">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label style="color:white;" class="col-sm-2 col-form-label">Login</label>
                        <div class="col-sm-10">
                            <input type="text" readonly="" style="color:white;" class="form-control-plaintext" id="staticEmail" value="<?php echo $infos->login; ?>">
                        </div>
                    </div>        
                    <a class="btn btn-primary btn-lg btn-block" href="?controller=user&action=requestEdit" style="color:white;">Modifier les informations</a> 
                </div> 
            </div>
            
            
        </div>
            
         </div>
         <?php
         }   

         public function render_successful_adress(){
             ?>
                <div class="alert alert-dismissible alert-success mb-0">
                    Adresse ajoutée avec succès! :)
                </div>

            <?php
         }
    }



?>
<?php
    namespace CSF\Views;

    use CSF\Models\UserModel;
    use CSF\Models\AdressModel;
    use CSF\Models\ShopModel;
    use CSF\Repositories\AdressRepository;
    use CSF\Repositories\ShopRepository;


    class BaseView {

        /**
        * @Inject
        * @var UserModel
        */
        private $userModel;

        /**
        * @Inject
        * @var AdressModel
        */
        private $adressModel;
        /**
        * @Inject
        * @var ShopModel
        */
        private $shopModel;
        /**
        * @Inject
        * @var AdressRepository
        */
        private $adressService;
        /**
        * @Inject
        * @var ShopRepository
        */
        private $shopService;
        

        public function __construct() {}

        public function render_navigation() {
            ?>

                <div class="bg-image" style=" ">

                <nav class="navbar navbar-expand-lg navbar-dark text-white" style="background-color: rgba(52,93,149,1)">
                    <a class="navbar-brand" href="?controller=user&action=home">Shop <img class="img-fluid" src="res/cartlogo.png" height="30" width="30"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarColor01">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                
                                    <?php 
                                        if (isset($_SESSION['user'])){
                                            echo "<a class='nav-link dropdown-toggle' data-toggle='dropdown' href='#' role='button' aria-haspopup='true' aria-expanded='false'> Compte </a>"; 
                                        } else {
                                            echo "<a class='nav-link' href='?controller=user&action=requestSignin'>Se connecter </a>";
                                        }
                                    ?>
                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    <a class="dropdown-item" href="?controller=user&action=profile">Mon profil</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="?controller=user&action=signout">Déconnexion</a>
                                </div>
                            </li>
                            <li> 
                                <?php 
                                        if (!isset($_SESSION['user'])){
                                            echo "<a class='nav-link' href='?controller=user&action=requestSignup'>S'inscrire </a>"; 
                                        } 
                                ?>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Magasiner</a>
                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    <a class="dropdown-item" href="?controller=shop&action=browse">Afficher le magasin</a>
                                    
                                    <?php if (isset($_SESSION['user'])) { ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="?controller=shop&action=showCart">Afficher le panier</a>
                                    <a class="dropdown-item" href="?controller=shop&action=showWishlist">Voir la liste de souhaits</a>
                                    <?php } ?>
                                </div>
                            </li>
                                                   
                            <li> 
                                <?php 
                                        if (isset($_SESSION['user']) && $_SESSION['user']->login == "admin"){
                                            echo "<a class='nav-link' href='?controller=shop&action=adminShowItems'>(ADMIN) Voir Articles </a>"; 
                                        } 
                                ?>
                            </li>
                        </ul>
                        <a class="nav-link">  
                            <?php 
                                if (isset($_SESSION['user'])){
                                    echo "Bienvenue, ". $_SESSION['user']->login; 
                                }
                             ?>
                        </a>  
                        <a class="nav-link" href="?controller=shop&action=showCart" style="color:white;">
                        <?php 
                                if (isset($_SESSION['user'])){
                                    echo '<img class="img-fluid" src="res/cartlogo.png" height="30" width="30">Panier (';
                                    echo sizeof($this->shopService->getAllCartItems($_SESSION['user']->login)).')'; 
                                    
                                }
                             ?>
                        </a>
                        <form class="form-inline my-2 my-lg-0" method="POST" action="?controller=shop&action=search">
                            <input class="form-control mr-sm-2" type="text" name="userSearch" placeholder="Search">
                            <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                        </form>

                        </form>
                    </div>
                    </div>
                </nav>
            <?php
        }

        public function render_copyright() {
            ?>
                <footer style="position:relative; bottom: 0; right: 0; left: 0;">
                    <div class="p-1 text-center" style="background-color:rgb(0,64,128);">
                        
                        <a href="#" style="color:white;"> Revenir au début de la page </a>
                    </div>
                    <div class="p-5 d-flex justify-content-center" style="background-color:rgb(0,33,66); color:white;">
                        <a class="btn btn-primary" href="#" style="color:white;">Nous contacter</a>
                        <a class="btn btn-primary" target="_blank" rel="noopener noreferrer" href="https://www.pornhub.com" style="color:white;">  Facebook</a>
                        Tous droits réservés - <?php echo date("Y"); ?> &copy;

                    </div>
                </div>
            <?php
        }

        public function render_messages(){
            
                ?>
                    
                            <?php if ($this->adressModel->getMessage() !== null){
                            ?><div class="alert alert-dismissible alert-success mb-0">           
                                <button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $this->adressModel->getMessage();?>
                            </div>
                           <?php  
                           } elseif ($this->shopModel->getMessage() !== null) {
                            ?><div class="alert alert-dismissible alert-info mb-0">           
                                    <button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $this->shopModel->getMessage();?>
                            </div>
                            <?php  
                           } 
                        
                    
                
                
        }

        protected function render_errors($errors = null){
			if ($errors != null) {
            ?>
                <div class="alert alert-dismissible alert-danger mb-0">
                    <?php foreach ($errors as $error) { ?>
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php print htmlentities($error); ?><br/>
                    <?php } ?>
                </div>
            <?php
            }
        }
        
    }
?>

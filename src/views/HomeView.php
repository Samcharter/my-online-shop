<?php
  namespace CSF\Views;

  use CSF\Repositories\UserRepository;
  use CSF\Repositories\ShopRepository;

  class HomeView extends BaseView {

    /**
        * @Inject
        * @var UserRepository
        */
        private $service;
        /**
        * @Inject
        * @var ShopRepository
        */
        private $shopService;

    


    public function __contruct() {}

    public function render(){
      $shopItems = $this->shopService->getAllItems('admin');
      $itemNumber = rand(0, sizeof($shopItems)- 1) ;



      parent::render_navigation();
      if (isset($_SESSION['user'])){ $infos = $this->service->getInfo($_SESSION['user']); }
      ?></div>

      
        <div class="container pt-5" style="background-image: url(res/bg2EDIT.png); background-repeat:no-repeat;">
          <div class="row d-flex justify-content-center pb-5">
          
            <div class="col-sm-4 p-2 text-center" style="color:white;">
              <h1 class="display-3">Bienvenue!</h1>
              <p class="lead" style="color:white;">Bienvenue dans notre magasin en ligne! Ici vous pourrez trouver tout ce dont vous avez besoin sans exception. Nous espérons pouvoir atteindre toutes vos attentes les plus folles!
              </p>
            </div>
            <div class="col-sm-4">
              <img src="res/homelogo.png" class="img-fluid" height="400" width="400">
            </div>
          </div>
        
          
        
          
          <div class="text-center ml-2 mr-2">
            
            <div class="row d-flex justify-content-center">
              <div class="col m-2" style=" background-color: white;">
                <?php if (isset($_SESSION['user'])){ 
                ?> <div class=' p-3 m-2 text-center'><b>Bonjour, <?php echo $infos->firstname ?>!</b><br>
                  <small>Membre depuis <?php echo $infos->subdate ?></small>
                  <div class='d-flex justify-content-center pt-2 pb-2'>                 
                    <a href='?controller=user&action=profile'><img src='res/profileicon.png' title='Accèder au profil' style='max-width: 150px; max-height: 150px;'></a>    
                  </div>
                  <a href='?controller=user&action=profile' class="btn btn-primary">Afficher le profil</a>
                
                </div>
                <?php } else {
                ?>
                <div class=' m-2 text-center'>
                  <h1 class='display-4'>Bienvenue!</h1><br>
                  
                  On dirait que vous n'êtes pas connectés! Veuillez vous connecter ou vous inscrire pour accèder à toutes les fonctions du site.<br>
                  <div class='btn-group p-2' role='group' aria-label='Basic example'>
                    <a class='btn btn-primary' href='?controller=user&action=requestSignin' style='color:white;'>Se connecter</a>
                   <a class='btn btn-primary' href='?controller=user&action=requestSignup' style='color:white;'>S'inscrire</a>
                  </div>
              
                </div><?php
            
              }
              ?>
              
              </div>
              <div class="col m-2" style=" background-color: white;">
              
                <div class=" m-2 pb-2 text-center">
                  Recommandé pour vous
                  <div class="container">
                      <a href="?controller=shop&action=showItem&page=home&id=<?php echo $shopItems[$itemNumber]->_id ?>" style="color: black; text-decoration: none;">
                        <h4><?php echo $shopItems[$itemNumber]->itemName ?></h4>                   
                            <img src="<?php echo $shopItems[$itemNumber]->itemPicURL[0] ?>" style="max-width: 150px;">                      
                      </a>
                  </div>              
                  <a href="?controller=shop&action=browse" class="btn btn-primary m-2">Voir des articles similaires</a>
                </div>
                  
                  
              </div>
              <div class="col m-2" style=" background-color: white;">
                <div class=" p-3 m-2 text-center">
                  <h4>Informations</h4>
                  Ce site web est construit en PHP et HTML avec l'aide de Bootstrap et utilise une base de données NoSQL; MongoDB.
                </div>
              </div>
              
            </div>
            <div class="row">
              <div class="container-fluid m-2" style=" background-color: white;">
                <div class="row">
                  <div class="col">
                    <a href="?controller=shop&action=browse&category=electroniques" class="lead" style="color: black;"><img src="res/categorylogo/electroniques.png" class="p-2" style="width: 150px;">Electroniques</a>
                  </div>
                  <div class="col">
                  <a href="?controller=shop&action=browse&category=sports" class="lead" style="color: black;"><img src="res/categorylogo/sports.png" class="p-2" style="width: 150px;">Sports</a>
                  </div>
                  <div class="col">
                  <a href="?controller=shop&action=browse&category=electromenagers" class="lead" style="color: black;"><img src="res/categorylogo/electromenagers.png" class="p-2" style="width: 150px;">Électroménagers</a>
                  </div>
                  <div class="col">
                  <a href="?controller=shop&action=browse&category=livres" class="lead" style="color: black;"><img src="res/categorylogo/Books.png" class="p-2" style="width: 150px;">Livres</a>
                  </div>
                  <div class="col">
                  <a href="?controller=shop&action=browse&category=meubles" class="lead" style="color: black;"><img src="res/categorylogo/meubles.png" class="p-2" style="width: 150px;">Meubles</a>
                  </div>
                  
                </div>
              </div>
            
            </div>
            <div class="row d-flex justify-content-center pb-5">
              <div class="m-2" style=" max-width: 46rem; width: 100%; background-color: white;">
              Yo
              
              </div>
              <div class="col m-2" style=" background-color: white;">
              Yo
              </div>
            </div>
          </div>
      </div>
        <?php
          parent::render_copyright();
    }

  }




 ?>

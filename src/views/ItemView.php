<?php
    namespace CSF\Views;

    use CSF\Repositories\AdressRepository;
    use CSF\Repositories\UserRepository;

    class ItemView extends BaseView{

        /**
        * @Inject
        * @var AdressRepository
        */
        private $adressService;

        /**
        * @Inject
        * @var UserRepository
        */
        private $userService;

        public function render($item){
            parent::render_navigation();
            parent::render_messages();
            
            ?>
            <script type = "text/javascript">
            
            </script>
            
            <div class="container-fluid pt-2" style="width:95%;">
                
                <div class="row" style="background-color: white; border-radius: 5px;">  
                    <div class="col-sm" style="margin-right: -2rem !important;">                        
                        <div class="p-3">
                            <table >
                                <tr>                                    
                                    <?php if (isset($item->itemPicURL[0])) { ?>
                                    <th><img id="0" src="<?php echo $item->itemPicURL[0] ?>" onmouseover='
                                        document.getElementById("toChange").src="<?php echo $item->itemPicURL[0] ?>";
                                        document.getElementById("0").style="border:solid 1px; border-color: rgba(0,0,0,0.2)";' 
                                        onmouseout="document.getElementById('0').style='border: 0px';" 
                                        width="75"></th>                                   
                                    <?php } ?>
                                    <th rowspan="4"><img id="toChange" class="pl-3"  src="<?php echo $item->itemPicURL[0] ?>" width="400" height="400"></th>                                    
                                </tr>
                                <tr>
                                    <?php if (isset($item->itemPicURL[1])) { ?>
                                    <td><img id="1" src="<?php echo $item->itemPicURL[1] ?>" onmouseover='
                                        document.getElementById("toChange").src="<?php echo $item->itemPicURL[1] ?>";
                                        document.getElementById("1").style="border:solid 1px; border-color: rgba(0,0,0,0.2)";' 
                                        onmouseout="document.getElementById('1').style='border: 0px';" 
                                        width="75"></td>                                   
                                    <?php } ?>
                                    </tr>
                                <tr>
                                    <?php if (isset($item->itemPicURL[2])) { ?>
                                    <td><img id="2" src="<?php echo $item->itemPicURL[2] ?>" onmouseover='
                                        document.getElementById("toChange").src="<?php echo $item->itemPicURL[2] ?>";
                                        document.getElementById("2").style="border:solid 1px; border-color: rgba(0,0,0,0.2)";' 
                                        onmouseout="document.getElementById('2').style='border: 0px';" 
                                        width="75"></td>                                   
                                    <?php } ?>                    
                                </tr>
                                <tr>
                                    <?php if (isset($item->itemPicURL[3])) { ?>
                                    <td><img id="3" src="<?php echo $item->itemPicURL[3] ?>" onmouseover='
                                        document.getElementById("toChange").src="<?php echo $item->itemPicURL[3] ?>";
                                        document.getElementById("3").style="border:solid 1px; border-color: rgba(0,0,0,0.2)";' 
                                        onmouseout="document.getElementById('3').style='border: 0px';" 
                                        width="75"></td>                                   
                                    <?php } ?>              
                                </tr>
                            </table>                                                              
                        </div>
                    </div>

                    <div class="col-6">
                        <h1 class="display-4" style="color: rgb(40,104,163);"><?php echo $item->itemName ?></h1>
                        <b ><?php echo $item->itemCategory ?></b><br>
                        Prix: <b style="color: rgb(40,104,163);">CDN$ <?php echo $item->itemPrice ?></b>
                        <p class="lead"><?php foreach($item->itemDescription as $description){
                            ?><li style="list-style: none;"><?php echo $description ?></li> <?php
                        } ?></p>
                    </div>
                    <div class="col-sm text-center m-2 p-2" style="border: solid 1px; border-color: rgba(0,0,0,0.1); border-radius: 5px;">
                        <b style="color: rgb(40,104,163);">CDN$ <?php echo $item->itemPrice ?></b><br>
                        <h4 style="color: rgb(34,147,21)">En stock.</h4>
                        <form method="POST" action="?controller=shop&action=addToCart&page=item&id=<?php echo $item->_id?>">
                            <select style="width: 70px;" class="custom-select" id="quantity" name="quantity">
                                
                                <option selected value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select><br>
                            <button class='btn btn-warning'>Ajouter au panier</button>
                            
                        </form>
                        <a href='?controller=shop&action=addToWishlist&page=item&id=<?php echo $item->_id ?>' style='color:white;' class='btn btn-primary'>Ajouter à la liste de souhaits</a>
                        <hr>
                        <?php
                               
                            if (isset($_SESSION['user'])){ 
                                $adresses = $this->adressService->getAll($_SESSION['user']->login);
                                if ($adresses != null) {
                        ?>
                        <ul style="list-style: none; text-align: left !important; ">
                            <li><b>Livrer à: </b><?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressFN; ?> <?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressLN; ?></li>
                            <li><?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressAD; ?></li>
                            <li><?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressPC; ?>, <?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressCT; ?>, <?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressPV; ?></li>
                            <li><?php echo $this->adressService->getAll($_SESSION['user']->login)[0]->adressCN; ?></li>
                            <li><b>Changer l'adresse de livraison</b></li>
                            <li><select id="adress">
                                <?php 
                                    foreach ($this->adressService->getAll($_SESSION['user']->login) as $adress){
                                        ?><option value="0"><?php echo $adress->adressAD ?></option><?php
                                    }
                                ?>
                                </select>
                            </li>
                            <li>Ajouter fonctionnalité</li>        
                        </ul>
                            <?php } 
                            else {
                                ?>
                                Vous n'avez aucune adresse de livraison inscrites à votre profil.
                                <br><a href="?controller=user&action=profile" class="btn btn-info">Ajouter une adresse</a>
                                <?php
                            }
                        } else {
                            ?> Veuillez vous connecter pour ajouter une adresse de livraison <br>
                            <a href="?controller=user&action=requestSignin" class="btn btn-primary">Se connecter</a><?php 
                        }?>
                        
                    </div> 
                </div>

                <div class="row mt-3" style="background-color: white; border-radius: 5px;">
                        Yo mec
                </div>
            </div>
        <?php }
    }

?>
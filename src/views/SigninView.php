<?php
    namespace CSF\Views;

    use CSF\Models\UserModel;
    
    class SigninView extends BaseView {

        /**
        * @Inject
        * @var UserModel
        */
        private $model;


        public function __construct() {}


        public function render() {
            parent::render_navigation();
            parent::render_messages();
            
            parent::render_errors($this->model->getErrors());
            ?>
                <div class="container pt-3" style=" height: 800px; color:white; background-image: url(res/bg2EDIT.png); background-repeat:no-repeat;">
                    <h3>Connexion</h3>
                    <form method="POST" action="?controller=user&action=signin">
                        <div class="form-group">
                            <label for="login">Nom d'utilisateur</label>
                            <input type="text" class="form-control" name="login" id="login">
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <button type="submit" class="btn btn-primary">Se connecter</button>
                    </form>
                </div>
            <?php
            parent::render_copyright();
        }
            
    }
    

    ?>
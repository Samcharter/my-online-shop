<?php

    namespace CSF\Views;

    use CSF\Repositories\AdressRepository;
    use CSF\Models\AdressModel;

    class AdressListView extends BaseView{
        /**
        * @Inject
        * @var AdressRepository
        */
        private $service;
        /**
        * @Inject
        * @var AdressModel
        */
        private $model;


        public function __construct(){}

        public function render(){
            parent::render_navigation();
            parent::render_messages();
            ?></div>
            <div class="container pt-3">
                <h2>Mon compte</h2>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="?controller=user&action=profile">Informations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="?controller=adress&action=show">Adresse(s)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">Achats</a>
                    </li>          
                </ul>
                <div class="jumbotron mt-2" style="background-image: url(res/bg2EDIT.png); background-repeat:no-repeat;">
                    <div class='row d-flex justify-content-center'>
            <?php
            
            
            $adresses = $this->service->getAll($_SESSION['user']->login);           
            $i = 1;
            if ($adresses != null){
                foreach ($adresses as $adress){
                    echo "<div class='jumbotron m-2 p-3 text-center' style='background-color:white; width:20rem'>                                                                                   
                    <h3 class='display-4 '>Adresse #$i</h3>
                    <ul class='text-left' style='list-style-type: none;'>
                        <li>".$adress->getAdressFN()."</li>
                        <li>".$adress->getAdressLN()."</li>
                        <li>".$adress->getAdressAD()."</li>
                        <li>".$adress->getAdressCT()."</li>
                        <li>".$adress->getAdressPC()."</li>
                        <li>".$adress->getAdressPV()."</li>
                        <li>".$adress->getAdressCN()."</li>
                    </ul>
                    <div class='btn-group p-2' role='group' aria-label='Basic example'>
                       <a class='btn btn-primary href='?controller=adress&action=edit&id=".$adress->getId()."'; style='color:white;'>Modifier</a></button>
                        <a class='btn btn-primary' href='?controller=adress&action=delete&id=".$adress->getId()."'; style='color:white;'>Supprimer</a></button>
                    </div></div>";
                    $i++;                                                                                                                                         
                }
            echo "<br><button type='button' class='btn btn-info btn-lg btn-block'><a href='?controller=adress&action=add' style='color:white;'>Ajouter une adresse</a></button>";
            } else {
                echo 
                "<div class='alert alert-warning'>
                    Vous n'avez aucune adresse à votre compte. <a href='?controller=adress&action=add'>Ajouter une adresse</a>                  
                </div>";
            } echo "</div></div>";
        }


    }
    ?>
<?php
    namespace CSF\Views;

    use CSF\Repositories\ShopRepository;

    class CartView extends BaseView {

        /**
        * @Inject
        * @var ShopRepository
        */
        private $service;

        public function __construct() { }

        public function render(){
            parent::render_navigation();
            parent::render_messages();
            $shopItems = $this->service->getAllItems('admin');
            $cartItems = $this->service->getAllCartItems($_SESSION['user']->login);
            $totalPrice = 0;
            $randomArray = @$this->randomSuggestionItemNumber();
            $title = "Cart";
            ?>
            <div class="container-fluid">
                <div class="row" >
                    <div class="col-10 mt-2">
                        <div class="container-fluid text-center pb-2" style="background-color:white; width: 100%;">
                            <h1 class="display-4 mute"><img src="res/emptyCart.png" class="img-fluid mr-2" style="width:65px; vertical-align: text-top; height:65px;"/>Votre panier</h1>
                            <?php 
                            if ($cartItems == null){
                                ?><p class="lead">Votre panier est vide!</p><?php 
                            }else {
                                ?> <hr><?php 
                            }
                            ?>
                            <div>
                                <?php $this->showAllCartItems(); ?>
                            </div>
                        </div>
                        
                            <div class="container-fluid mt-2" style="background-color: white;">
                                <div class="m-2 p-3">
                                    <h2 class="font-weight-bold text-center">Recommendations</h2>
                                </div>
                                <div class="row mr-2 ml-1">
                                    <div class="col-sm p-2 ml-1 mr-1 mb-3 " style="background-color:rgb(232,232,232); border-radius: 5px">
                                    
                                        <div class="text-center">
                                        <a href="?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[0]]->_id ?>" style="color: black; text-decoration: none;">
                                            <h4 class="font-weight-lighter"><?php echo $shopItems[$randomArray[0]]->itemName; ?></h4>
                                            <img src="<?php echo $shopItems[$randomArray[0]]->itemPicURL[0]; ?>" height="200px" width="200px">
                                            <br>
                                            <b>Prix: </b><?php echo $shopItems[$randomArray[0]]->itemPrice; ?>$
                                            <form method="POST" action="?controller=shop&action=addToCart&page=cart&id=<?php echo $shopItems[$randomArray[0]]->_id?>">
                                                <b>Qt: </b>
                                                <select id="quantity" name="quantity">    
                                                    <option selected value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select> 
                                                <div class='p-2' role='group' aria-label='Basic example'>
                                                    <button class='btn btn-warning'>Ajouter</button>
                                                    <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[0]]->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                                                </div>
                                            </form>
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-sm p-2 ml-1 mr-1 mb-3" style="background-color:rgb(232,232,232); border-radius: 5px">
                                        <div class="text-center">
                                        <a href="?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[1]]->_id ?>" style="color: black; text-decoration: none;">
                                            <h4 class="font-weight-lighter"><?php echo $shopItems[$randomArray[1]]->itemName; ?></h4>
                                            <img src="<?php echo $shopItems[$randomArray[1]]->itemPicURL[0]; ?>" height="200px" width="200px">
                                            <br>
                                            <b>Prix: </b><?php echo $shopItems[$randomArray[1]]->itemPrice; ?>$
                                            <form method="POST" action="?controller=shop&action=addToCart&page=cart&id=<?php echo $shopItems[$randomArray[1]]->_id?>">
                                                <b>Qt: </b>
                                                <select id="quantity" name="quantity">    
                                                    <option selected value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select> 
                                                <div class='p-2' role='group' aria-label='Basic example'>
                                                    <button class='btn btn-warning'>Ajouter</button>
                                                    <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[1]]->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                                                </div>
                                            </form>
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-sm p-2 ml-1 mr-1 mb-3" style="background-color:rgb(232,232,232); border-radius: 5px">
                                        <div class="text-center">
                                        <a href="?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[2]]->_id ?>" style="color: black; text-decoration: none;">
                                            <h4 class="font-weight-lighter"><?php echo $shopItems[$randomArray[2]]->itemName; ?></h4>
                                            <img src="<?php echo $shopItems[$randomArray[2]]->itemPicURL[0]; ?>" height="200px" width="200px">
                                            <br>
                                            <b>Prix: </b><?php echo $shopItems[$randomArray[2]]->itemPrice; ?>$
                                            <form method="POST" action="?controller=shop&action=addToCart&page=cart&id=<?php echo $shopItems[$randomArray[2]]->_id?>">
                                                <b>Qt: </b>
                                                <select id="quantity" name="quantity">    
                                                    <option selected value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select> 
                                                <div class='p-2' role='group' aria-label='Basic example'>
                                                    <button class='btn btn-warning'>Ajouter</button>
                                                    <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[2]]->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                                                </div>
                                            </form>
                                        </a>
                                        </div>
                                        </div>
                                        <div class="col-sm p-2 ml-1 mr-1 mb-3" style="background-color:rgb(232,232,232); border-radius: 5px">
                                        <div class="text-center">
                                        <a href="?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[3]]->_id ?>" style="color: black; text-decoration: none;">
                                            <h4 class="font-weight-lighter"><?php echo $shopItems[$randomArray[3]]->itemName; ?></h4>
                                            <img src="<?php echo $shopItems[$randomArray[3]]->itemPicURL[0]; ?>" height="200px" width="200px">
                                            <br>
                                            <b>Prix: </b><?php echo $shopItems[$randomArray[3]]->itemPrice; ?>$
                                            <form method="POST" action="?controller=shop&action=addToCart&page=cart&id=<?php echo $shopItems[$randomArray[3]]->_id?>">
                                                <b>Qt: </b>
                                                <select id="quantity" name="quantity">    
                                                    <option selected value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select>
                                                <div class='p-2' role='group' aria-label='Basic example'>
                                                    <button class='btn btn-warning'>Ajouter</button>
                                                    <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[3]]->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                                                </div>
                                           </form>
                                        </a>
                                        </div>
                                    </div>
                                    <div class="col-sm p-2 ml-1 mr-1 mb-3" style="background-color:rgb(232,232,232); border-radius: 5px">
                                        <div class="text-center">
                                        <a href="?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[5]]->_id ?>" style="color: black; text-decoration: none;">
                                            <h4 class="font-weight-lighter"><?php echo $shopItems[$randomArray[4]]->itemName; ?></h4>
                                            <img src="<?php echo $shopItems[$randomArray[4]]->itemPicURL[0]; ?>" height="200px" width="200px">
                                            <br>
                                            <b>Prix: </b><?php echo $shopItems[$randomArray[4]]->itemPrice; ?>$
                                            <form method="POST" action="?controller=shop&action=addToCart&page=cart&id=<?php echo $shopItems[$randomArray[4]]->_id?>">
                                                <b>Qt: </b>
                                                <select id="quantity" name="quantity">    
                                                    <option selected value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select> 
                                                <div class='p-2' role='group' aria-label='Basic example'>
                                                    <button class='btn btn-warning'>Ajouter</button>
                                                    <a href='?controller=shop&action=showItem&page=shop&id=<?php echo $shopItems[$randomArray[4]]->_id ?>' style='color:white;' class='btn btn-primary'>En savoir plus</a>
                                                </div>
                                            </form>
                                        </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-2 pb-2">
                                    <div class="container-fluid" style=" background-color: rgb(232,232,232); border-radius: 5px;">
                                        <div class="row text-center">
                                        <div class="col">
                                            <a href="?controller=shop&action=browse&category=electroniques" class="lead" style="color: black;"><img src="res/categorylogo/electroniques.png" class="p-2" style="width: 150px;"><br>Electroniques</a>
                                        </div>
                                        <div class="col">
                                        <a href="?controller=shop&action=browse&category=#" class="lead" style="color: black;"><img src="res/categorylogo/sports.png" class="p-2" style="width: 150px;"><br>Sports</a>
                                        </div>
                                        <div class="col">
                                        <a href="?controller=shop&action=browse&category=#" class="lead" style="color: black;"><img src="res/categorylogo/electromenagers.png" class="p-2" style="width: 150px;"><br>Électroménagers</a>
                                        </div>
                                        <div class="col">
                                        <a href="?controller=shop&action=browse&category=#" class="lead" style="color: black;"><img src="res/categorylogo/Books.png" class="p-2" style="width: 150px;"><br>Livres</a>
                                        </div>
                                        <div class="col">
                                        <a href="?controller=shop&action=browse&category=meubles" class="lead" style="color: black;"><img src="res/categorylogo/meubles.png" class="p-2" style="width: 150px;"><br>Meubles</a>
                                        </div>
                                        
                                        </div>
                                    </div>
                                    
                                    </div>
                                
                            </div>
                            
                        

                    </div>
                
                    <div class="col mt-2 mr-2 pr-2 pl-2" >
                        <div class="container-fluid text-center pb-2" style="background-color:white; width: 100%; border-radius: 5px;">
                            <?php 
                                
                                
                                ?>
                                <table class="table table-hover">
                                <thead>
                                    <h1 class="display-4 text-center">Prix</h1>
                                </thead>
                                <?php 
                                foreach($cartItems as $item){
                                    ?>
                                    
                                        <tr>
                                            <td class="text-left">
                                                <b><?php echo $item->itemName ?> x<?php echo $item->qt?></b>
                                            </td>
                                            <td class="text-right">
                                                Prix: <?php echo $item->qt * $item->itemPrice; //* +$item->itemPrice;
                                                $totalPrice = $totalPrice + $item->qt * ($item->itemPrice);
                                                
                                                 ?>$
                                            </td>
                                        </tr>
                                    <?php
                                }
                            ?>
                                </table>
                        
                        <b class="p-2">Prix total:</b> <?php echo $totalPrice; ?>$<br>
                        <a class='btn btn-block btn-success mt-2 <?php if ($this->service->getAllCartItems($_SESSION['user']->login) == null){ echo("disabled"); }  ?>' href='?controller=shop&action=#' style='color:white;'>Acheter<img class="img-fluid" src="res/cartlogo.png" height="20" width="20"></a>
                        </div>

                        <div class="container-fluid mt-2" style="background-color: white;">
                                Allo
                        </div>
                    </div>                   
                </div>
                
            </div>
            

            <?php
            parent::render_copyright();
        }

        public function showAllCartItems(){
            $items = $this->service->getAllCartItems($_SESSION['user']->login);
            
            ?>
            <table>
            <?php 
            foreach($items as $item){
                ?>
                <tr>
                    <div class="media mb-4">
                        <form action="?controller=shop&action=editQt&id=<?php echo $item->_id ?>" method="POST">
                            <div class="media-left">
                            <a href='?controller=shop&action=showItem&page=cart&id=<?php echo $item->_id ?>' style="text-decoration: none; color: black;"><img  src="<?php echo $item->itemPicURL[0] ?>" style="height: 125px; width: 125px;" /></a>
                            </div>
                            <div class="media-body">
                                
                                <ul class='text-left' style='list-style-type: none;'>
                                    <li><a href='?controller=shop&action=showItem&page=cart&id=<?php echo $item->_id ?>' style="text-decoration: none; color: black;"><h5 class="media-heading"><?php echo $item->itemName ?></h5></a></li>
                                    <li><?php echo $item->itemDescription[0].", ".$item->itemDescription[1].", ".$item->itemDescription[2]."."; ?></li>
                                    <li><b>Quantité: </b>
                                        <select id="qt" name="qt">     
                                            <option <?php if($item->qt == 1){ echo "selected"; } ?> value="1">1</option>
                                            <option <?php if($item->qt == 2){ echo "selected"; } ?> value="2">2</option>
                                            <option <?php if($item->qt == 3){ echo "selected"; } ?> value="3">3</option>
                                        </select>
                                        
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="media-right" style="position: relative" >
                                <ul class='text-right' style='list-style-type: none;'>
                                    <li><b>Prix:</b> <?php echo $item->itemPrice;?></li><?php 
                                    if ($item->qt > 1){
                                        ?><li><b>Prix total:</b> <?php echo $item->qt * $item->itemPrice;?> </li>
                                        <?php 
                                    }?></ul>
                                    <div class='btn-group pt-3 mb-0' role='group' style="">
                                            <button class='btn btn-primary' style='color:white;'>Mettre à jour</button>
                                            <a class='btn btn-warning' href='?controller=shop&action=deleteFromCart&id=<?php echo $item->_id ?>' style='color:white;'>Supprimer</a>
                                    </div>
                                    
                                
                            </div>
                        </form>
                    </div>
                </tr>
                
                <?php 
            }
            ?>
            </table>
            <?php
            

        }

        public function randomSuggestionItemNumber(){
            $shopItems = $this->service->getAllItems('admin'); 
            $cartItems = $this->service->getAllCartItems($_SESSION['user']->login);
            $numbers = null;
            $suggestions = null;
            $i = 0;
            $k = 0;
            
            //check if item is in cart
            while (sizeof($cartItems) > $i){
                
                for($j = 0; $j < sizeof($shopItems); $j++ ){    
                    if($cartItems[$i]->itemName == $shopItems[$j]->itemName){
                        $numbers[] = $j;
                    }
                }
                $i++;

            }
            //create array with removed cartItems from shopItems
            sort($numbers);
            $shopItemsArray = null;
            for ($l = 0; $l < sizeof($shopItems); $l++){
                $shopItemsArray[] = $l;
            }           
            for ($m = 0; $m < sizeof($numbers); $m++){
                unset($shopItemsArray[$numbers[$m]]);
            }
            shuffle($shopItemsArray);
            return($shopItemsArray);
        }
        
    }


?>
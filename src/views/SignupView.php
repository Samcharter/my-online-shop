<?php
    namespace CSF\Views;

    use CSF\Models\UserModel;

    class SignupView extends BaseView {

        /**
        * @Inject
        * @var UserModel
        */
        private $model;
        
        public function __construct() {}

        public function render() {
            parent::render_navigation();
            parent::render_errors($this->model->getErrors());
       
            ?>
            
                <div class="container pt-3 pb-5" style="height: 800px;background-image: url(res/bg2EDIT.png); background-repeat:no-repeat; color: white;">
                    <h3>Inscription</h3>
                    <form method="POST" id="signUpForm" action="?controller=user&action=signup">
                        <div class="form-group">
                            <label for="signin">Nom d'utilisateur</label>
                            <input type="text" class="form-control" name="login" id="login" value="<?php print $_POST['login'] ?? '' ?>" required />
                        </div>
                        <div class="form-group">
                            <label for="firstname">Prénom</label>
                            <input type="text" class="form-control" name="firstname" id="firstname" value="<?php print $_POST['firstname'] ?? '' ?>" required />
                        </div>
                        <div class="form-group">
                            <label for="lastname">Nom</label>
                            <input type="text" class="form-control" name="lastname" id="lastname" value="<?php print $_POST['lastname'] ?? '' ?>" required />
                        </div>
                        <div class="form-group">
                            <label for="email">Courriel</label>
                            <input type="email" class="form-control" name="email" id="emailField" required value="<?php print $_POST['email'] ?? '' ?>" required />
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" class="form-control" name="password" id="password" required />
                        </div>
                        <div class="form-group">
                            <label for="confirmpassword">Confirmation du mot de passe</label>
                            <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" required />
                        </div>
                        <button class="btn btn-primary" id="okButton" >S'inscrire</button>
                    </form>
                </div>
            <?php
            parent::render_copyright();
        }
    }

?>

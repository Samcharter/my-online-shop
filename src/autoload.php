<?php
    # Vendor librairies in Docker container
    require_once __DIR__ . "/../vendor/autoload.php";

    # Own classes
    require_once __DIR__ . "/Router.php";
    require_once __DIR__ . "/Controllers/adressController.php";
    require_once __DIR__ . "/Controllers/UserController.php";
    require_once __DIR__ . "/Controllers/ShopController.php";
    require_once __DIR__ . "/Models/AdressModel.php";
    require_once __DIR__ . "/Models/UserModel.php";
    require_once __DIR__ . "/Models/ShopModel.php";
    require_once __DIR__ . "/Repositories/Database.php";
    require_once __DIR__ . "/Repositories/Adress.php";
    require_once __DIR__ . "/Repositories/AdressRepository.php";
    require_once __DIR__ . "/Repositories/User.php";
    require_once __DIR__ . "/Repositories/UserRepository.php";
    require_once __DIR__ . "/Repositories/Shop.php";
    require_once __DIR__ . "/Repositories/ShopRepository.php";
    require_once __DIR__ . "/Views/BaseView.php";
    require_once __DIR__ . "/Views/ProfileView.php";
    require_once __DIR__ . "/Views/HomeView.php";
    require_once __DIR__ . "/Views/SigninView.php";
    require_once __DIR__ . "/Views/SignUpView.php";
    require_once __DIR__ . "/Views/AdressView.php";
    require_once __DIR__ . "/Views/AdressListView.php";
    require_once __DIR__ . "/Views/ShopView.php";
    require_once __DIR__ . "/Views/CartView.php";
    require_once __DIR__ . "/Views/ItemView.php";
   
    
?>

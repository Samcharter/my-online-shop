<?php
  $builder = new \DI\ContainerBuilder();
  $builder->useAutowiring(true);
  $builder->useAnnotations(true);
  $container = $builder->build();
  $router = $container->get(CSF\Router::class);
  $router->process();
  ?>

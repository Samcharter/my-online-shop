<?php
    namespace CSF\Models;

    use CSF\Repositories\Shop;
    use CSF\Repositories\User;
    
    class ShopModel{

        /**
        * @Inject
        * @var Shop
        */
        private $shop;
        /**
        * @Inject
        * @var User
        */
        private $user;
        private $message;

        public function __construct(){}

        public function getUser(){ return $this->user; }
        public function getShop(){ return $this->shop; }
        public function getMessage() { return $this->message; }

        public function setUser($value) { $this->user = $value; }
        public function setShop($value) { $this->shop = $value; }
        public function setMessage($value) { $this->message = $value; }
        
        
            
    }
?>
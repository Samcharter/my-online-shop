<?php
    namespace CSF\Models;

    use CSF\Repositories\User;
    use CSF\Repositories\UserRepository;

    class UserModel {
        /**
        * @Inject
        * @var User
        */
        private $user;
        /**
        * @Inject
        * @var UserRepository
        */
        private $service;
        private $errors;

        public function __construct(){}
        
        public function getUser() { return $this->user; }
        public function getErrors() { return $this->errors; }

        public function setUser($value) { $this->user = $value; }
        public function setErrors($value) { $this->errors = $value; }

        function validate(){
            $this->errors = null;
            
            if (!filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL)) {
                $this->errors[] = "Vous devez entrer une adresse courriel valide.";
            }
            if (strlen($this->user->getPassword()) < 8) {
                $this->errors[] = "Vous devez entrer un mot de passe d'au moins 8 caractères.";
            }
            if (strlen($this->user->getConfirmpassword()) < 8) {
                $this->errors[] = "Vous devez entrer une confirmation de mot de passe d'au moins 8 caractères.";
            }
            if ($this->user->getPassword() !== $this->user->getConfirmpassword()) {
                $this->errors[] = "Les mots de passe doivent être identiques.";
            }
            if (sizeof($this->service->getAll($this->user->getLogin())) !== 0){
                $this->errors[] = "Votre nom d'utilisateur est déjà pris.";         
            }
            if (sizeof($this->service->getAllEmail($this->user->getEmail())) !== 0){
                $this->errors[] = "Un compte avec cette adresse e-mail existe déjà.";
                        
            }
        }







    }
    
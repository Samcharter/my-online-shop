<?php 
    namespace CSF\Models;

    use CSF\Repositories\Adress;
    use CSF\Repositories\User;

    class AdressModel {

        /**
        * @Inject
        * @var Adress
        */
        private $adress;
        /**
        * @Inject
        * @var User
        */
        private $user;
        
        private $adressList;
        private $errors;
        private $message;

        public function __construct(){}

        public function getUser() { return $this->user; }
        public function getAdress() { return $this->adress; }
        public function getAdressList() { return $this->adressList; }
        public function getErrors() { return $this->errors; }
        public function getMessage() { return $this->message; }

        public function setUser($value) { $this->user = $value; }
        public function setAdress($value) { $this->adress = $value; }
        public function setAdressList($value) { $this->adressList = $value; }
        public function setErrors($value) { $this->errors = $value; }
        public function setMessage($value) { $this->message = $value; }
    }



?>
